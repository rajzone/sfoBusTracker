import React, {StyleSheet, Dimensions, PixelRatio} from "react-native";
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);

export default StyleSheet.create({
    "body": {
        "fontFamily": "\"Helvetica Neue\", Helvetica, Arial, sans-serif"
    },
    "html": {
        "background": "none"
    },
    "map path": {
        "fill": "none",
        "stroke": "#35143C",
        "strokeLinejoin": "round",
        "strokeLinecap": "round"
    },
    "map freeways": {
        "stroke": "#F56476",
        "strokeWidth": 1.5
    },
    "map neighborhoods": {
        "stroke": "#D2A1EE"
    },
    "map streets": {
        "stroke": "#9A7DAD"
    },
    "map arteries": {
        "stroke": "black",
        "strokeWidth": 1.5
    },
    "logoHead": {
        "fontFamily": "'Anton', sans-serif"
    },
    "dropDown": {
        "width": "30%"
    },
    "hover": {
        "fill": "yellow"
    },
    "sliderContainer": {
        "textAlign": "center",
        "position": "relative",
        "top": 600
    },
    "selR: hover": {
        "fill": "red"
    },
    "h1": {
        "position": "absolute",
        "marginLeft": "42%",
        "paddingTop": "2%",
        "color": "#8B008B",
        "fontSize": 1.3,
        "fontWeight": "100"
    },
    "h2": {
        "position": "absolute",
        "marginLeft": "42%",
        "paddingTop": "2%",
        "color": "#8B008B",
        "fontSize": 1,
        "fontWeight": "100",
        "top": 30
    },
    "h4": {
        "fontSize": 1
    },
    "label": {
        "fontSize": 1,
        "fontWeight": "100"
    },
    "inputsAll": {
        "backgroundColor": "#4ebb53",
        "border": 1,
        "color": "white",
        "textAlign": "center",
        "textDecoration": "none",
        "display": "inline-block",
        "fontSize": 0.75
    },
    "inputsNone": {
        "backgroundColor": "#d47c7c",
        "border": 1,
        "color": "white",
        "textAlign": "center",
        "textDecoration": "none",
        "display": "inline-block",
        "fontSize": 0.75
    }
});