# sfo Bus Tracker
Live visualization of SF Muni map based on
http://webservices.nextbus.com/service/publicJSONFeed.

SF Muni vehicle locations on top of the map, dynamically updating their locations every 15 seconds. You can get this information from the NextBus real-time data feed. The documentation is here:
http://www.nextbus.com/xmlFeedDocs/NextBusXMLFeed.pdf

## Installation
```
npm install
npm start
```
```
## Open
http://localhost:8080/
